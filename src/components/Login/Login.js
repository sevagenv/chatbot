import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import "./style.css";

function Login() {
  const [username, setUsername] = useState("");
  const [pass, setPass] = useState("");
  const history = useHistory();

  function handleUser(evt) {
    setUsername(evt.target.value);
  }

  function handlePass(evt) {
    setPass(evt.target.value);
  }

  function handleSubmit() {
    history.push({
      pathname: "/chatbot",
      state: { name: username },
    });
  }

  return (
    <div class="log-form">
      <h2>Login to your account</h2>
      <form>
        <input
          type="text"
          value={username}
          onChange={handleUser}
          placeholder="Username"
          title="username"
        />
        <input
          type="password"
          value={pass}
          onChange={handlePass}
          title="username"
          placeholder="password"
        />
        <button type="submit" class="btn" onClick={handleSubmit}>
          Login
        </button>
      </form>
    </div>
  );
}

export default Login;
