import React from "react";
import "./App.css";
import Routes from "../components/Routes/Routes";

function App() {
  return (
    <div>
      <header className="App-header">
        <Routes />
      </header>
    </div>
  );
}

export default App;
