import React, { useState } from "react";
import Bot from "./Bot";
import "./bot.css";
import BotButton from "./display.svg";

function BotWrapper() {
  const [showBot, setShowBot] = useState(true);

  function handleClick() {
    setShowBot(!showBot);
  }

  return (
    <div>
      <div className="bot">{showBot ? <Bot /> : <div></div>}</div>
      <input
        className="chat_butt"
        type="image"
        alt="button for chat"
        src={BotButton}
        onClick={handleClick}
      ></input>
    </div>
  );
}

export default BotWrapper;
